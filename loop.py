for i in range(0,32):
    print("5'd%d: begin"%(i));
    print("      s5c_crcdata256 <= {{%d{8'h00},s5_crc_n_1,%d{8'h00}};"%(32-i,i-4));
    print("      s5c_remainder <= 32'h00000000;");
    print("end;");
