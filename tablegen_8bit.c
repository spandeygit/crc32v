#include <stdio.h>
#include <stdbool.h>

// tables arrays for lookup fast
static bool __TABLE_INIT= false;
static unsigned int TABLE_EV[256];

unsigned char reverse(unsigned char b) {
   b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
   b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
   b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
   return b;
}

unsigned int Compute_CRC32_Simple(unsigned char * bytes, int n)
{
    const unsigned int polynomial = 0x04C11DB7; /* divisor is 32bit */
    unsigned int crc = 0xffffffff; /* CRC value is 32bit */
    unsigned char b;
    unsigned char bk;

    for (int j=0; j<n; j++)
    {
	b = bytes[j];
	b = reverse(b);
        crc ^= (unsigned int)(b << 24); /* move byte into MSB of 32bit CRC */
        bk = (crc >>24) & 0x0ff;

        for (int i = 0; i < 8; i++)
        {
            if ((crc & 0x80000000) != 0) /* test for MSB = bit 31 */
            {
                crc = (unsigned int)((crc << 1) ^ polynomial);
            }
            else
            {
                crc <<= 1;
            }
        }
        //printf("1..Computing for %x %x is %x\n",b,bk,crc);
    }


    crc = crc ^ 0xffffffff;
    // NOW REVERSE COMPLETE 32 bit WORD BIT BY BIT 
    crc = 
	        reverse((crc & 0xff000000)>>24)   + 
		(reverse((crc & 0x00ff0000)>>16)<<8)   + 
		(reverse((crc & 0x0000ff00)>>8)<<16)   + 
		(reverse((crc & 0x000000ff))<<24)    
	;

    return crc;
}

unsigned int CRC32_Table(unsigned char  * bytes, int n)
{
    const unsigned int polynomial = 0x04C11DB7; /* divisor is 32bit */
    unsigned int crc = 0xffffffff; /* CRC value is 32bit */
    unsigned char b;
    unsigned char bk;

	if (__TABLE_INIT) {
		// nothing to do - follow by lookup
	} else {
		// init all tables first 
        	for (int j = 0; j < 256; j++){
		     b = j; crc = 0;
                     crc ^= (unsigned int)(b << 24);  //move byte into MSB of 32bit CRC 
                     for (int k = 0; k < 8; k++)
                     {
                         if ((crc & 0x80000000) != 0)  //test for MSB = bit 31 
                         {
                             crc = (unsigned int)((crc << 1) ^ polynomial);
                         }
                         else
                         {
                             crc <<= 1;
                         }
                     }
		     // now add value to table
		     TABLE_EV[j] = crc;
		} // j 0-255
	        __TABLE_INIT = true;
	}
        crc = 0xffffffff; /* CRC value is 32bit */

        for (int j=0; j<n; j++)
        {
            b = bytes[j];
            b = reverse(b); bk = b;
            b = b ^ ((crc>>24) & 0x0ff);
            crc = (crc<<8) ^ TABLE_EV[b]; 
            //printf("2..Computing for %x %x is %x\n",bk,b,crc);

        }

        crc = crc ^ 0xffffffff;
        // NOW REVERSE COMPLETE 32 bit WORD BIT BY BIT 
        crc = 
	        reverse((crc & 0xff000000)>>24)   + 
		(reverse((crc & 0x00ff0000)>>16)<<8)   + 
		(reverse((crc & 0x0000ff00)>>8)<<16)   + 
		(reverse((crc & 0x000000ff))<<24) ;


	return crc;
}

int main(int argc, char *argv[]) {
  	unsigned char inp[36] ;
	unsigned int crc;
	for (int i =0;i<33;i++){
		inp[i] = i;
	}
	crc = Compute_CRC32_Simple(inp,33);
	printf("Computed CRC is %x \n",crc);

	for (int i =0;i<33;i++){
		inp[i] = i;
	}
	crc = CRC32_Table(inp,33);
	printf("Computed MEMORY LOOKUP CRC is %x \n",crc);

}
