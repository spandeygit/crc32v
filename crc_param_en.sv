`timescale 1ns/10ps

module crc_param_en
#(
  parameter k_POLY = 64'ha12ed66fa674fd33,     // CRC64A
  //parameter k_POLY = 64'had20303e8a480b1d,   // CRC64B
  parameter k_POLY_LENGTH = 64,
  parameter k_WIDTH_IN = 512
  )
  (
   input rst,
   input clk,
   input [k_WIDTH_IN-1:0] data_in,
   input crc_en,
   output reg [k_POLY_LENGTH-1:0] crc_out
   );
  
  reg     rst_d1, rst_d2;
  reg     crc_en_d1, crc_en_d2;
  reg [k_POLY_LENGTH-1:0] data_next_d2;
  reg [k_POLY_LENGTH-1:0][3:0] data_next_d1;
  reg [k_POLY_LENGTH-1:0][k_WIDTH_IN-1:0] data_mask;
  
  // function to calculate crc_next iteratively
  function reg [k_POLY_LENGTH-1:0] crc_next (reg [k_WIDTH_IN-1:0] data, reg [k_POLY_LENGTH-1:0] crc_state, reg [k_POLY_LENGTH-1:0] poly, reg reset);
    reg [k_POLY_LENGTH-1:0] crc_new;

    crc_next = crc_state;
    for (int i=0;i<k_WIDTH_IN;i++)
      begin
        for (int j=1;j<k_POLY_LENGTH;j++)
          begin
            if(poly[j] == 1'b1)
              crc_new[j] = (i==0 && reset) ? data[i] :
                                    crc_next[j-1] ^ crc_next[k_POLY_LENGTH-1] ^ data[i];
            else
              crc_new[j] = (i==0 && reset) ? 1'b1 : crc_next[j-1];
          end
        crc_new[0] = (i==0 && reset) ? ~data[i] : data[i] ^ crc_next[k_POLY_LENGTH-1];
        crc_next = crc_new;
      end // for (int i=0;i<k_WIDTH_IN;i++)
  endfunction
  
  // The data portion of the CRC is precalculated and pipelined
  // The XOR tree is split up to help 100G (512-wide) reliably get over 315 Mhz.
  always_ff @(posedge clk)
    begin
        for (int i=0;i<k_POLY_LENGTH;i++)
          begin
            if(crc_en)
              begin
                data_next_d1[i][0] <= ^(data_mask[i][k_WIDTH_IN*1/4-1:0] & data_in[k_WIDTH_IN/4-1:0]);
                data_next_d1[i][1] <= ^(data_mask[i][k_WIDTH_IN*2/4-1:k_WIDTH_IN*1/4] & data_in[k_WIDTH_IN*2/4-1:k_WIDTH_IN*1/4]);
                data_next_d1[i][2] <= ^(data_mask[i][k_WIDTH_IN*3/4-1:k_WIDTH_IN*2/4] & data_in[k_WIDTH_IN*3/4-1:k_WIDTH_IN*2/4]);
                data_next_d1[i][3] <= ^(data_mask[i][k_WIDTH_IN*4/4-1:k_WIDTH_IN*3/4] & data_in[k_WIDTH_IN*4/4-1:k_WIDTH_IN*3/4]);
              end
            // this allows us to work with less than 64 byte packets, but means that we can't handle back to back 64 byte packets
            // which is ok, because we wouldn't have anywhere to put the FCS, so these won't happen
            if(crc_en_d1)
              data_next_d2[i] <= ^data_next_d1[i];  
          end
    end

  always_ff @(posedge clk)
    begin
      if(crc_en)
        rst_d1 <= rst;
      if(crc_en_d1)
        rst_d2 <= rst_d1;
      
      crc_en_d1 <= crc_en;
      crc_en_d2 <= crc_en_d1;
    end
  
  always_ff @(posedge clk)
    begin
      if(crc_en_d2)
        crc_out <= crc_next('0, crc_out, k_POLY, rst_d2) ^ data_next_d2;
    end // always @ (posedge clk)
  

  // This create a big constant table that is used as a mask for
  // the incomming parallel data before computing the XOR
  // This breaks out the data portion of the XOR from the reset of the CRC
  // which can be pipelined.
  // This table is created by starting the CRC state at 0, then feeding in a single 'd1
  // Then running the CRC for each data bit.  This doesn't result in any logic
  // being generated, the table is just used elsewhere where logic is created.
  always_comb
    begin
      reg [k_POLY_LENGTH-1:0] mask_new, mask_next;

      mask_next = '0;
      for (int i=0;i<k_WIDTH_IN;i++)
        begin
          for (int j=1;j<k_POLY_LENGTH;j++)
            begin
              if(k_POLY[j] == 1'b1)
                mask_new[j] = mask_next[j-1] ^ mask_next[k_POLY_LENGTH-1] ^ ((i==0) ? 1'b1 : 1'b0);
              else
                mask_new[j] = mask_next[j-1];
          end
          mask_new[0] = ((i==0) ? 1'b1 : 1'b0) ^ mask_next[k_POLY_LENGTH-1];
          mask_next = mask_new;

          // Each bit in mask_next corresponds to a CRC term that has this data bit in it.
          // Since we want a vector mask that we can apply to the data for each CRC bit
          // to say which bits to XOR, we need to transpose this
          for (int y=0;y<k_POLY_LENGTH;y++)
            data_mask[y][i] = mask_next[y];
        end
  end // begin
  
endmodule
