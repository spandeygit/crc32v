for i in range(0,32):
    print("crctab_ev0 s2lookup_%d"%(i));
    print("(");
    print(".clk(i_clk),");
    print(".rstn(i_areset_n),");
    print(".addr(s6_crcdata256[%d:%d]),"%((((i+1)*8)-1),i*8));
    print(".rdata(s6c_crccrc_%d)"%(i));
    print(");");
    
for i in range(0,32):
    print("s7_crccrc_%d <= s6c_crccrc_%d;"%(i,i));


for i in range(0,32):
    print("reg [31:0] s7_crccrc_%d ;"%(i));
    print("wire [31:0] s6c_crccrc_%d;"%(i));

print("reg [31:0] s8_crccrc_a, s8_crccrc_b, s8_remainder, s8_crc_data; ");
print("      s8_crc_a <= ");
for i in range(0,16):
    print("              ^ s7_crc_%d "%(i));
print("      ;");


print("      s8_crc_b <= ");
for i in range(16,32):
    print("              ^ s7_crc_%d "%(i));
print("      ;");

"""
crctab_ev0 lookup1_0
(
.clk(i_clk),
.rstn(i_areset_n),
.addr({s1_data256[0], s1_data256[1],  s1_data256[2],  s1_data256[3],  s1_data256[4], s1_data256[5],  s1_data256[6],  s1_data256[7]} ),
.rdata(s1c_data_crc_0[31:0])
);
"""