`timescale 1ns/10ps
`default_nettype none

module txfcs_256
 (
  input wire          clk,
  input wire          a_reset,
  input wire          sof_in,          // pulses high at start of frame
  input wire          eof_in,          // pulses high at end of frame
  input wire [4:0]    tag_in,          // location of the last byte of the packet
  input wire [255:0]  data_in,         // data to compute FCS
  input wire          valid_in,        // data will be ignored without valid
  output reg          sof_out,
  output reg          eof_out,
  output reg [4:0]    tag_out,
  output reg [255:0]  data_out,
  output reg          valid_out
  );


  reg           sof_d1;
  reg           eof_d1;
  reg [4:0]     tag_d1;
  reg [255:0]   data_d1;
  reg           valid_d1;

  reg           sof_d2;
  reg           eof_d2;
  reg [4:0]     tag_d2;
  reg [255:0]   data_d2;
  reg           valid_d2;

  reg           sof_d3;
  reg           eof_d3;
  reg [4:0]     tag_d3;
  reg [255:0]   data_d3;
  reg [127:0]   data_crc_d3;
  reg           valid_d3;
  
  reg           sof_d4;
  reg           eof_d4;
  reg [4:0]     tag_d4;
  reg [255:0]   data_d4;
  reg [63:0]    data_crc_d4;
  reg           valid_d4;

  reg           sof_d5;
  reg           eof_d5;
  reg [4:0]     tag_d5;
  reg [255:0]   data_d5;
  reg [31:0]    data_crc_d5;
  reg           valid_d5;

  reg           sof_d6;
  reg           eof_d6;
  reg [4:0]     tag_d6;
  reg [255:0]   data_d6;
  reg [15:0]    data_crc_d6;
  reg           valid_d6;

  reg           sof_d7;
  reg           eof_d7;
  reg [4:0]     tag_d7;
  reg [255:0]   data_d7;
  reg           valid_d7;

  reg           sof_d8;
  reg           eof_d8;
  reg [4:0]     tag_d8;
  reg [255:0]   data_d8;
  reg           valid_d8;

  reg           sof_d9;
  reg           eof_d9;
  reg [4:0]     tag_d9;
  reg [255:0]   data_d9;
  reg           valid_d9;

  reg           sof_d10;
  reg           eof_d10;
  reg [4:0]     tag_d10;

  reg [255:0]   mask_d2;  
  wire [31:0]   crc_d4, crc_d5, crc_d6, crc_d7, crc_d8, crc_d9;

  reg [31:0]    fcs_d9, fcs_d10;

  
  // order reverse bits in each byte 
  function reg [255:0] byte_reverse256(reg [255:0] data);
      for (integer by=0; by<32; by++)
        for (integer bi=0; bi<8; bi++)
          byte_reverse256[by*8+bi] = data[by*8+7-bi];
    endfunction

  function reg [31:0] byte_reverse32(reg [31:0] data);
      for (integer by=0; by<4; by++)
        for (integer bi=0; bi<8; bi++)
          byte_reverse32[by*8+bi] = data[by*8+7-bi];
    endfunction
  
  always_ff @(posedge clk)
    begin
      sof_d1 <= sof_in & valid_in;
      eof_d1 <= eof_in & valid_in;
      tag_d1 <= tag_in;
      data_d1 <= byte_reverse256(data_in);
      valid_d1 <= valid_in;

      sof_d2 <= sof_d1;
      eof_d2 <= eof_d1;
      // for patial ends of packet, add 1 to set enables for partial crc blocks
      // otherwise we always leave disabled.
      tag_d2 <= eof_d1 ? tag_d1 + 1'b1 : '0;  
      mask_d2 <= eof_d1 ? ({256{1'b1}}<< ((8'd31-tag_d1)*4'd8)) : '1;   // calculate mask to zero past the EOF
      data_d2 <= data_d1;
      valid_d2 <= valid_d1;

      sof_d3 <= sof_d2;
      eof_d3 <= eof_d2;
      tag_d3 <= tag_d2;
      data_d3 <= data_d2 & mask_d2;                                     // apply mask since CRC is just OR'd in later
      data_crc_d3 <= tag_d2[4] ? data_d2[127:0] : data_d2[255:128];
      valid_d3 <= valid_d2;

      sof_d4 <= sof_d3;
      eof_d4 <= eof_d3;
      tag_d4 <= tag_d3;
      data_d4 <= byte_reverse256(data_d3);                           // put the data back in the order it came
      data_crc_d4 <= tag_d3[3] ? data_crc_d3[63:0] : data_crc_d3[127:64];
      valid_d4 <= valid_d3;
      
      sof_d5 <= sof_d4;
      eof_d5 <= eof_d4;
      tag_d5 <= tag_d4;
      data_d5 <= data_d4;
      data_crc_d5 <= tag_d4[2] ? data_crc_d4[31:0] : data_crc_d4[63:32];
      valid_d5 <= valid_d4;
      
      sof_d6 <= sof_d5;
      eof_d6 <= eof_d5;
      tag_d6 <= tag_d5;
      data_d6 <= data_d5;
      data_crc_d6 <= tag_d5[1] ? data_crc_d5[15:0] : data_crc_d5[31:16];
      valid_d6 <= valid_d5;
      
      sof_d7 <= sof_d6;
      eof_d7 <= eof_d6;
      tag_d7 <= tag_d6;
      data_d7 <= data_d6;
      valid_d7 <= valid_d6;

      sof_d8 <= sof_d7;
      eof_d8 <= eof_d7;
      tag_d8 <= tag_d7 - 1'b1;   // put the tag back to the original
      data_d8 <= data_d7;
      valid_d8 <= valid_d7;

      sof_d9 <= sof_d8;
      eof_d9 <= eof_d8;
      tag_d9 <= tag_d8;
      data_d9 <= data_d8;
      valid_d9 <= valid_d8;

      sof_d10 <= sof_d9;
      eof_d10 <= eof_d9;
      tag_d10 <= tag_d9;
    end
  

  crc_param_en
    #(
      .k_WIDTH_IN(256),
      .k_POLY_LENGTH(32),
      .k_POLY(32'h04C11DB7)
      )
      crc_256
      (
       .data_in(data_d1),
       .crc_en(valid_d1),
       .crc_out(crc_d4),
       .rst(sof_d1),
       .clk(clk)
       );

  crc_param_nofb
    #(
      .k_WIDTH_IN(128),
      .k_POLY_LENGTH(32),
      .k_POLY(32'h04C11DB7)
      )
      crc_128
      (
       .data_in(data_d2[255:128]),     // data needs to come in early to the pipeline to precalculate data portion of CRC
       .crc_en(tag_d3[4]&valid_d3),             // enable this block for the last word of data (1 clock before crc_d4 is finished)
       .crc_in(crc_d4),
       .crc_out(crc_d5),
       .rst(1'b0),                    // never reset
       .clk(clk)
       );

  crc_param_nofb
    #(
      .k_WIDTH_IN(64),
      .k_POLY_LENGTH(32),
      .k_POLY(32'h04C11DB7)
      )
      crc_64
      (
       .data_in(data_crc_d3[127:64]),
       .crc_en(tag_d4[3]&valid_d4),
       .crc_in(crc_d5),
       .crc_out(crc_d6),
       .rst(1'b0),            // never reset
       .clk(clk)
       );

  crc_param_nofb
    #(
      .k_WIDTH_IN(32),
      .k_POLY_LENGTH(32),
      .k_POLY(32'h04C11DB7)
      )
      crc_32
      (
       .data_in(data_crc_d4[63:32]),
       .crc_en(tag_d5[2]&valid_d5),
       .crc_in(crc_d6),
       .crc_out(crc_d7),
       .rst(1'b0),            // never reset
       .clk(clk)
       );

  crc_param_nofb
    #(
      .k_WIDTH_IN(16),
      .k_POLY_LENGTH(32),
      .k_POLY(32'h04C11DB7)
      )
      crc_16
      (
       .data_in(data_crc_d5[31:16]),
       .crc_en(tag_d6[1]&valid_d6),
       .crc_in(crc_d7),
       .crc_out(crc_d8),
       .rst(1'b0),            // never reset
       .clk(clk)
       );

  crc_param_nofb
    #(
      .k_WIDTH_IN(8),
      .k_POLY_LENGTH(32),
      .k_POLY(32'h04C11DB7)
      )
      crc_8
      (
       .data_in(data_crc_d6[15:8]),
       .crc_en(tag_d7[0]&valid_d7),
       .crc_in(crc_d8),
       .crc_out(crc_d9),
       .rst(1'b0),            // never reset
       .clk(clk)
       );

  always_ff @(posedge clk)
    begin
      // crc is _d9 when the first stage is all that is needed, it's _d8 for most cases
      fcs_d9 <= byte_reverse32(crc_d9) ^ 32'hffffffff;  // per IEEE802.3 spec
      fcs_d10 <= fcs_d9;
    end

  // Logic to append the FCS
  always_ff @(posedge clk)
    begin
      if(eof_d10 && tag_d10 > 'd27)  // special case, if the CRC spills into the this clock
        begin
          // for full last clock, the ending comes from the first crc block, 1 clock later
          // otherwise it comes from the partial crc chain
          data_out <= (tag_d10 == 'd31) ? {fcs_d9,224'd0} : 
                      {(fcs_d10 << {~tag_d10[1:0],3'b000}),224'd0};
          eof_out <= 1'b1;
          tag_out <= {3'b000,tag_d10[1:0]};  // tag can be only 0,1,2, or 3
          valid_out <= 1'b1;
          sof_out <= 1'b0;
        end
      else if(eof_d9)                   // most cases, EOF stays in the same clock as EOF
        begin
          data_out <= data_d9 | ({fcs_d9,224'd0}>> (({1'b0,tag_d9}+1'd1)*8)); 
          valid_out <= 1'b1;
          sof_out <= 1'b0;
          if (tag_d9 < 'd28)  // eof this clock
            begin
              eof_out <= 1'b1;
              tag_out <= tag_d9 + 3'd4;
            end
          else               // eof next clock
            begin
              eof_out <= 1'b0;
              tag_out <= '0;
            end
        end // if (eof_d9)
      else
        begin
          sof_out <= sof_d9;
          eof_out <= 1'b0;
          tag_out <= '0;
          data_out <= data_d9;
          valid_out <= valid_d9;
        end 
    end // always_ff @ (posedge clk)
  
endmodule // txfcs_256
`default_nettype wire
